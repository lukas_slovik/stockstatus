﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class TransactionHistoryDao
    {
        private List<TR_HIST> historyData;
        private LD_DET piece;
        public TransactionHistoryDao(LD_DET piece)
        {
            this.historyData = null;
            this.piece = piece;
        }

        private void GetHistoryData()
        {
            if (this.piece != null)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from history in Context.TR_HIST
                                       where history.TR_PART.ToUpper().Trim() == this.piece.LD_PART.ToUpper().Trim() &&
                                       history.TR_SERIAL.ToUpper().Trim() == this.piece.LD_LOT.ToUpper().Trim() &&
                                       history.TR_REF.ToUpper().Trim() == this.piece.LD_REF.ToUpper().Trim()
                                       orderby history.TR_TRNBR
                                       select history;
                    try
                    {
                        this.historyData = requestQuery.ToList<TR_HIST>();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        private bool DataAvailable()
        {
            if (this.piece != null)
            {
                if (this.piece.LD_PART != null && this.piece.LD_LOT != null && this.piece.LD_REF != null)
                {
                    if (this.historyData == null)
                        GetHistoryData();
                    return (this.historyData != null);
                }
            }
            return false;
        }

        public string GetPo()
        {
            if (DataAvailable())
            {
                TR_HIST poHist = this.historyData.Find(hist => hist.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.ReceivePOTransactionType.Trim().ToUpper());
                if (poHist != null)
                    return poHist.TR_NBR;
                else
                {
                    poHist = this.historyData.Find(hist => hist.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.ReceivePOTransactionUnplanned.Trim().ToUpper());
                    if (poHist != null)
                        return Properties.Settings.Default.ReceivePOTransactionUnplanned;
                }
            }
            return "";
        }

        public bool MovedToProduction()
        {
            if (DataAvailable())
            {
                int notInCn = 0;
                List<TR_HIST> cnHist = this.historyData.FindAll(hist => hist.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.ReceiveTransactionType.Trim().ToUpper()
                && (hist.TR_PROGRAM.Trim().ToUpper() == Properties.Settings.Default.CnToProdProgram.ToUpper().Trim() ||
                hist.TR_PROGRAM.Trim().ToUpper() == Properties.Settings.Default.CnToProdProgramManual.ToUpper().Trim()));
                if (cnHist != null)
                {
                    if (cnHist.Count > 0)
                    {
                        foreach (TR_HIST cnOp in cnHist)
                        {
                            TR_HIST issTr = this.historyData.Find(transaction => transaction.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.IssueTransactionType.Trim().ToUpper()
                              && transaction.TR_LOT == cnOp.TR_LOT);
                            notInCn += (int)ConsignmentTransactionValidator.Check(issTr, cnOp);
                        }
                        return (notInCn <= 0);
                    }
                }
            }
            return false;
        }

        public bool HasCNOperation()
        {
            if (DataAvailable())
            {
                List<TR_HIST> cnHist = this.historyData.FindAll(hist => hist.TR_TYPE.Trim().ToUpper() == Properties.Settings.Default.ReceiveTransactionType.Trim().ToUpper()
                && (hist.TR_PROGRAM.Trim().ToUpper() == Properties.Settings.Default.CnToProdProgram.ToUpper().Trim() ||
                hist.TR_PROGRAM.Trim().ToUpper() == Properties.Settings.Default.CnToProdProgramManual.ToUpper().Trim()));
                if (cnHist != null)
                    return (cnHist.Count > 0);
            }
            return false;
        }

        public bool? IsConsignment()
        {
            if (DataAvailable())
            {
                if (PredefinedParams.IsConsignmentGroup(ItemNumberDao.Instance.ItemGroup(this.piece.LD_PART)))
                {
                    PoType po = PredefinedParams.IsConsignmentPO(GetPo());
                    if (po == PoType.CN)
                    {
                        return (!MovedToProduction());
                    }
                    else if (po == PoType.Inconclusive)
                    {
                        if (HasCNOperation())
                            return (!MovedToProduction());
                    }
                }
            }
            return null;
        }
    }

}
