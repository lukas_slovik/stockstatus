﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class ConsignmentTransactionValidator
    {
        private static bool IsCnLoc(TR_HIST transaction)
        {
            return (transaction.TR_STATUS.ToUpper().Trim() == Properties.Settings.Default.ConsignmentStatus.ToUpper().Trim());
        }

        public static ConsignmentState Check(TR_HIST issTr, TR_HIST rctTr)
        {
            if (issTr != null && rctTr != null)
            {
                if (IsCnLoc(issTr))
                {
                    if (!IsCnLoc(rctTr))
                    {
                        //odstranene z cn
                        return ConsignmentState.IsNonCn;
                    }
                }
                else
                {
                    if (IsCnLoc(rctTr))
                    {
                        //vratene na CN
                        return ConsignmentState.IsCN;
                    }
                }
            }
            return ConsignmentState.Unknown;
        }
    }
}
