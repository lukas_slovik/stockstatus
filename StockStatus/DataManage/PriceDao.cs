﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class PriceDao:TimeRelevant
    {
        private static PriceDao instance;

        public static PriceDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new PriceDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.PriceRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.allPrices == null);
            }
        }

        private List<Coste> allPrices;
        private object allPricesLock;
        private PriceDao()
        {
            this.allPricesLock = new object();
            this.allPrices = null;
        }

        public List<Coste> AllPrices
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new MFGPRO_SQLEntities())
                    {
                        var requestQuery = from price in Context.Costes
                                           select price;
                        try
                        {
                            lock (this.allPricesLock)
                            {
                                this.allPrices = requestQuery.ToList<Coste>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                lock (this.allPricesLock)
                {
                    if (this.allPrices != null)
                        return new List<Coste>(this.allPrices);
                }
                return new List<Coste>();
            }
        }

        public double GetItemPrice(string itemNumber)
        {
            if (itemNumber != null)
            {
                Coste itemPrice= AllPrices.Find(price=>price.Matricula.ToUpper().Trim()== itemNumber.ToUpper().Trim());
                if (itemPrice != null)
                    if (itemPrice.Coste1 != null)
                        return (double)itemPrice.Coste1;
            }
            return 0;
        }
    }
}
