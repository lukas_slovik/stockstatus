﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class StockStatusM
    {
        public ObservableCollection<StockData> Graph { get; set; }
        public object GraphLock { get; private set; }
        public ObservableCollection<StockDataTable> Table { get; set; }
        public object TableLock { get; private set; }
        private object selectedItem = null;
        public object SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                // selected item has changed
                selectedItem = value;
            }
        }

        public StockStatusM()
        {
            GraphLock = new object();
            TableLock = new object();
        }
    }
}
