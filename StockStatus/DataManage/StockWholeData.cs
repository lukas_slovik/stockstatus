﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class StockWholeData
    {
        public LD_DET QadPiece { get; private set; }
        public string Group { get; set; }
        public bool IsConsignmnet { get; set; }
        public StockWholeData(LD_DET qadPiece)
        {
            QadPiece = qadPiece;
            IsConsignmnet = false;
            Group = "";
        }
    }
}
