﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public abstract class TimeRelevant
    {
        protected abstract int TimeOutPeriod { get; }
        protected abstract bool NotInitialized { get; }
        private DateTime lastUpdated;
        protected void Updated(DateTime newTime)
        {
            this.lastUpdated = newTime;
        }
        protected bool ShouldRefresh
        {
            get
            {
                if (NotInitialized)
                    return true;
                if ((DateTime.Now - lastUpdated).TotalSeconds > TimeOutPeriod)
                    return true;
                return false;
            }
        }
        public TimeRelevant()
        {
            this.lastUpdated = DateTime.Now;
        }
    }
}
