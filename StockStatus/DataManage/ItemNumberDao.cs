﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class ItemNumberDao : TimeRelevant
    {
        private static ItemNumberDao instance;

        public static ItemNumberDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new ItemNumberDao();
                return instance;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.ItemNumberRefreshTime;
            }
        }

        protected override bool NotInitialized
        {
            get
            {
                return (this.allItems == null);
            }
        }

        private List<Matricula> allItems;
        private object allItemsLock;
        private ItemNumberDao()
        {
            this.allItems = null;
            this.allItemsLock = new object();
        }

        public List<Matricula> Items()
        {
            if (ShouldRefresh)
            {
                using (var Context = new MFGPRO_SQLEntities())
                {
                    var requestQuery = from item in Context.Matriculas
                                       select item;
                    try
                    {
                        lock (this.allItemsLock)
                        {
                            this.allItems = requestQuery.ToList<Matricula>();
                            Updated(DateTime.Now);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            lock (this.allItemsLock)
            {
                if (this.allItems != null)
                    return new List<Matricula>(this.allItems);
            }
            return new List<Matricula>();
        }

        public Matricula GetByItemNumber(string itemNumber)
        {
            if (itemNumber != null)
            {
                List<Matricula> allItems = Items();
                return allItems.Find(item=>item.Matricula1.ToUpper().Trim()== itemNumber.ToUpper().Trim());
            }
            return null;
        }

        public string ItemGroup(string itemNumber)
        {
            if (itemNumber != null)
            {
                List<Matricula> allItems = Items();
                Matricula foundItem= allItems.Find(item => item.Matricula1.ToUpper().Trim() == itemNumber.ToUpper().Trim());
                if (foundItem != null)
                    return foundItem.TipoArticulo.ToUpper().Trim();
            }
            return "";
        }
    }
}
