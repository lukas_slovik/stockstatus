﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public enum ConsignmentState
    {
        IsCN=1,
        IsNonCn=-1,
        Unknown=0
    }
}
