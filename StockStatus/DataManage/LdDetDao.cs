﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class LdDetDao
    {
        private static LdDetDao instance;

        public static LdDetDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new LdDetDao();
                return instance;
            }
        }

        private LdDetDao()
        {

        }

        public List<LD_DET> StockData()
        {
            using (var Context = new MFGPRO_SQLEntities())
            {
                var requestQuery = from stock in Context.LD_DET
                                   select stock;
                try
                {
                    return requestQuery.ToList<LD_DET>();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return new List<LD_DET>();
        }

        public List<LD_DET> ClearedStockData()
        {
            List<LD_DET> clearedStock = new List<LD_DET>();
            List<LD_DET> wholeStock= StockData();
            foreach(LD_DET stockItem in wholeStock)
            {
                if (stockItem.LD_QTY_OH != null)
                    if (stockItem.LD_QTY_OH != 0)
                        clearedStock.Add(stockItem);
            }
            return clearedStock;
        }
    }
}
