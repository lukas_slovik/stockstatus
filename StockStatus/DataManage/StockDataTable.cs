﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class StockDataTable
    {
        public string Group { get; private set; }
        public double Amount { get; private set; }
        public double PercentualWithoutCN { get; private set; }
        public double PercentualWithCN { get; private set; }
        private List<StockWholeData> stockData { get; set; }
        public StockDataTable(string group, List<StockWholeData> stockData, double totalNonCNStock, double cNStockTotal)
        {
            Group = group;
            this.stockData = stockData;
            Amount = 0;
            foreach (StockWholeData piece in stockData)
            {
                if (piece.QadPiece.LD_QTY_OH != null)
                    Amount += (double)piece.QadPiece.LD_QTY_OH * PriceDao.Instance.GetItemPrice(piece.QadPiece.LD_PART);
            }
            if (group != Properties.Settings.Default.RAWCNTitle)
                PercentualWithoutCN = Math.Round(Amount / (totalNonCNStock / 100), Properties.Settings.Default.AmountDecimalCount);
            else
                PercentualWithoutCN = 0;
            PercentualWithCN = Math.Round(Amount / ((totalNonCNStock + cNStockTotal) / 100), Properties.Settings.Default.AmountDecimalCount);
            Amount = Math.Round(Amount, Properties.Settings.Default.AmountDecimalCount);

        }
    }
}
