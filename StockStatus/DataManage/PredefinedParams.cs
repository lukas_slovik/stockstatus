﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class PredefinedParams
    {
        public static bool IsConsignmentGroup(string itemGroup)
        {
            string allGroups = Properties.Settings.Default.ConsignmentGroups;
            if (allGroups != null)
            {
                string[] separatedGroups = allGroups.Split(Properties.Settings.Default.ListSeparator);
                foreach(string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == itemGroup.Trim().ToUpper())
                        return true;
                }
            }
            return false;
        }

        public static PoType IsConsignmentPO(string poNumber)
        {
            string allPoNumbers = Properties.Settings.Default.ConsignmentPO;
            if (poNumber == "")
                return PoType.Inconclusive;
            if (allPoNumbers != null)
            {
                string[] separatedGroups = allPoNumbers.Split(Properties.Settings.Default.ListSeparator);
                foreach (string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == poNumber.Trim().ToUpper())
                        return PoType.CN;
                }
            }
            return PoType.NonCN;
        }

        public static bool IsUnfinishedFinfLocation(string location)
        {
            string allLocations = Properties.Settings.Default.UnfinishedFinFLocation;
            if (allLocations != null)
            {
                string[] separatedGroups = allLocations.Split(Properties.Settings.Default.ListSeparator);
                foreach (string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == location.Trim().ToUpper())
                        return true;
                }
            }
            return false;
        }

        public static bool IsRAWGroup(string itemGroup)
        {
            string allGroups = Properties.Settings.Default.RAWItemGroups;
            if (allGroups != null)
            {
                string[] separatedGroups = allGroups.Split(Properties.Settings.Default.ListSeparator);
                foreach (string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == itemGroup.Trim().ToUpper())
                        return true;
                }
            }
            return false;
        }

        public static bool IsWIPGroup(string itemGroup)
        {
            string allGroups = Properties.Settings.Default.WIPItemGroups;
            if (allGroups != null)
            {
                string[] separatedGroups = allGroups.Split(Properties.Settings.Default.ListSeparator);
                foreach (string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == itemGroup.Trim().ToUpper())
                        return true;
                }
            }
            return false;
        }

        public static bool ISFgGroup(string itemGroup)
        {
            string allGroups = Properties.Settings.Default.FGItemGroups;
            if (allGroups != null)
            {
                string[] separatedGroups = allGroups.Split(Properties.Settings.Default.ListSeparator);
                foreach (string group in separatedGroups)
                {
                    if (group.Trim().ToUpper() == itemGroup.Trim().ToUpper())
                        return true;
                }
            }
            return false;
        }
    }
}
