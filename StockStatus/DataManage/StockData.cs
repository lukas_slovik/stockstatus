﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage
{
    public class StockData
    {
        public string Group { get; private set; }
        public double Amount { get; private set; }
        public double AmountPercent { get; private set; }
        private List<StockWholeData> stockData { get; set; }
        public StockData(string group, List<StockWholeData> stockData, double totalStock)
        {
            Group = group;
            this.stockData = stockData;
            Amount = 0;
            foreach (StockWholeData piece in stockData)
            {
                if (piece.QadPiece.LD_QTY_OH != null)
                    Amount += (double)piece.QadPiece.LD_QTY_OH * PriceDao.Instance.GetItemPrice(piece.QadPiece.LD_PART);
            }
            AmountPercent = Math.Round(Amount / (totalStock / 100), Properties.Settings.Default.AmountDecimalCount);
            Amount = Math.Round(Amount, Properties.Settings.Default.AmountDecimalCount);

        }
    }
}
