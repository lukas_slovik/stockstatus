﻿using StockStatus.DataManage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.Reports
{
    public class ReportDataUpdater
    {
        private static void CreateHeaderData(StockStatusReportData reportData, Guid connectionId, StockStatusM stockData)
        {
            double totalStock = 0;
            double totalStockNonCn = 0;
            foreach (StockDataTable group in stockData.Table)
            {
                if (group.Group != "RAW_CN")
                    totalStockNonCn += group.Amount;
                totalStock += group.Amount;
            }
            reportData.HeaderInfo.AddHeaderInfoRow(DateTime.Now.ToString(), connectionId.ToString(), totalStock, totalStockNonCn);
        }

        private static void CreateTableData(StockStatusReportData reportData, ObservableCollection<StockDataTable> table, Guid connectionId)
        {
            foreach (StockDataTable stockTableData in table)
            {
                reportData.TableData.AddTableDataRow(stockTableData.Group, stockTableData.Amount, stockTableData.PercentualWithoutCN, stockTableData.PercentualWithCN, connectionId.ToString());
            }
        }

        private static void CreateGraphData(StockStatusReportData reportData, ObservableCollection<StockData> graph, Guid connectionId)
        {
            foreach (StockData stockGraphData in graph)
            {
                reportData.GraphData.AddGraphDataRow(stockGraphData.Group, stockGraphData.AmountPercent, stockGraphData.Amount.ToString(), connectionId.ToString());
            }
        }

        private static void Export(StockStatusReportData reportData, string exportPath)
        {
            ReportPrint reportControl = new ReportPrint(exportPath);
            reportControl.PrintToFile(reportData);
        }

        public static void ExportStockReport(string exportPath, StockStatusM stockData)
        {
            Guid connectionId = Guid.NewGuid();
            StockStatusReportData reportData = new StockStatusReportData();
            CreateHeaderData(reportData, connectionId, stockData);
            CreateTableData(reportData, stockData.Table, connectionId);
            CreateGraphData(reportData, stockData.Graph, connectionId);
            Export(reportData, exportPath);
        }
    }
}
