﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.Reports
{
    public class ReportPrint
    {
        private ReportDocument cryRpt;
        private bool LoadReport(string reportName)
        {
            if (System.IO.File.Exists(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + Properties.Settings.Default.ReportsLocation + reportName))
            {
                try
                {
                    this.cryRpt.Load(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + Properties.Settings.Default.ReportsLocation + reportName, CrystalDecisions.Shared.OpenReportMethod.OpenReportByDefault);
                    return true;
                }
                catch (Exception e)
                {
                    if (e.InnerException != null)
                        Console.WriteLine(e.Message);
                    else
                        Console.WriteLine(e.Message + " " + e.InnerException.Message);
                }
            }
            return false;
        }

        private bool ExportToDisk()
        {
            try
            {
                if (this.exportPath != null)
                    cryRpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, this.exportPath);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private bool SetReportData(DataSet reportData)
        {
            try
            {
                this.cryRpt.SetDataSource(reportData);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool PrintToFile(DataSet reportData)
        {
            if (LoadReport(Properties.Settings.Default.StockStatusReportName))
                if (SetReportData(reportData))
                    if (ExportToDisk())
                        return true;
            return false;
        }

        private string exportPath;
        public ReportPrint(string exportPath)
        {
            this.exportPath = exportPath + "\\StockReport_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".pdf";
            try
            {
                cryRpt = new ReportDocument();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
