﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.MVC
{
    public class LockedRunnableQueue
    {
        private Queue<Runnable> runnableList = new Queue<Runnable>();
        private object listLock = new object();
        public void Add(Runnable task)
        {
            lock (this.listLock)
            {
                this.runnableList.Enqueue(task);
            }
        }
        public Runnable Get()
        {
            lock (this.listLock)
            {
                if (this.runnableList.Count > 0)
                    return this.runnableList.Dequeue();
                else
                    return null;
            }
        }
    }
}
