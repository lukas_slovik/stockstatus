﻿using StockStatus.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.MVC.Tasks
{
    public class SaveStockStatusReport : Runnable
    {
        private MainController controller;
        private string selectedPath;
        public SaveStockStatusReport(MainController mvcController, string selectedPath) : base(mvcController)
        {
            this.controller = mvcController;
            this.selectedPath = selectedPath;
        }

        public override void StopTask()
        {
        }

        protected override void RunResult()
        {
            this.controller.ReportExported();
        }

        protected override void RunTask()
        {
            if (this.selectedPath != null)
                ReportDataUpdater.ExportStockReport(this.selectedPath, this.controller.StockData);
            TaskCompleted();
        }
    }
}
