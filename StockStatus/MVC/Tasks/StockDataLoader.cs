﻿using StockStatus.DataManage;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.MVC.Tasks
{
    public class StockDataLoader : Runnable
    {
        private MainController controller;
        private List<StockWholeData> stockCounted;
        private double nonCNStockTotal;
        private double cNStockTotal;
        public StockDataLoader(MainController mvcController) : base(mvcController)
        {
            this.controller = mvcController;
            this.nonCNStockTotal = 0;
            this.cNStockTotal = 0;
        }

        public override void StopTask()
        {

        }

        protected override void RunResult()
        {
            this.controller.StockData.Graph.Clear();
            this.controller.StockData.Table.Clear();
            var groups = this.stockCounted.GroupBy(group => group.Group).Select(grp => new
            {
                Group = grp.Key
            }).ToList();
            foreach (var group in groups)
            {
                List<StockWholeData> groupedStock = this.stockCounted.FindAll(piece => piece.Group == group.Group);
                if (group.Group != Properties.Settings.Default.RAWCNTitle)
                    this.controller.StockData.Graph.Add(new StockData(group.Group, groupedStock, nonCNStockTotal));
                this.controller.StockData.Table.Add(new StockDataTable(group.Group, groupedStock, nonCNStockTotal, cNStockTotal));
            }
            this.controller.DataLoaded();
        }

        private void StockGroup(StockWholeData newPiece)
        {
            string itemGroup = ItemNumberDao.Instance.ItemGroup(newPiece.QadPiece.LD_PART);
            if (PredefinedParams.IsRAWGroup(itemGroup))
            {
                if (newPiece.IsConsignmnet)
                    newPiece.Group = Properties.Settings.Default.RAWCNTitle;
                else
                    newPiece.Group = Properties.Settings.Default.RAWGroupTitle;
            }
            else if (PredefinedParams.ISFgGroup(itemGroup))
            {
                if (PredefinedParams.IsUnfinishedFinfLocation(newPiece.QadPiece.LD_LOC))
                    newPiece.Group = Properties.Settings.Default.WIPGroupTitle;
                else
                    newPiece.Group = Properties.Settings.Default.FGGroupTitle;
            }
            else if (PredefinedParams.IsWIPGroup(itemGroup))
            {
                newPiece.Group = Properties.Settings.Default.WIPGroupTitle;
            }
            else
            {
                newPiece.Group = itemGroup;
            }
        }

        private void ConsignmentCheck(StockWholeData newPiece)
        {
            TransactionHistoryDao historyData = new TransactionHistoryDao(newPiece.QadPiece);
            bool? isCons = historyData.IsConsignment();
            if (isCons != null)
                newPiece.IsConsignmnet = (bool)isCons;
        }

        protected override void RunTask()
        {
            this.stockCounted = new List<StockWholeData>();
            List<LD_DET> stock = LdDetDao.Instance.ClearedStockData();
            foreach (LD_DET piece in stock)
            {
                StockWholeData newPiece = new StockWholeData(piece);
                if (piece.LD_LOT.ToUpper().Trim() == "Z1707270043")
                    Console.WriteLine();
                ConsignmentCheck(newPiece);
                StockGroup(newPiece);
                if (!newPiece.IsConsignmnet)
                    this.nonCNStockTotal += (double)newPiece.QadPiece.LD_QTY_OH * PriceDao.Instance.GetItemPrice(newPiece.QadPiece.LD_PART);
                else
                    this.cNStockTotal += (double)newPiece.QadPiece.LD_QTY_OH * PriceDao.Instance.GetItemPrice(newPiece.QadPiece.LD_PART);
                this.stockCounted.Add(newPiece);
            }
            DataTable debugData = new DataTable();
            debugData.Columns.Add("LD_PART", typeof(string));
            debugData.Columns.Add("LD_LOT", typeof(string));
            debugData.Columns.Add("LD_LOC", typeof(string));
            debugData.Columns.Add("LD_DATE", typeof(string));
            debugData.Columns.Add("LD_QTY_OH", typeof(string));
            debugData.Columns.Add("LD_REF", typeof(string));
            debugData.Columns.Add("LD_SITE", typeof(string));
            debugData.Columns.Add("LD_STATUS", typeof(string));
            debugData.Columns.Add("LD_QTY_ALL", typeof(string));
            debugData.Columns.Add("Group", typeof(string));
            debugData.Columns.Add("IsConsignmnet", typeof(string));
            foreach (StockWholeData newPiece in this.stockCounted)
            {
                debugData.Rows.Add(newPiece.QadPiece.LD_PART,
                    newPiece.QadPiece.LD_LOT,
                    newPiece.QadPiece.LD_LOC,
                    newPiece.QadPiece.LD_DATE.ToString(),
                    newPiece.QadPiece.LD_QTY_OH.ToString(),
                    newPiece.QadPiece.LD_REF,
                    newPiece.QadPiece.LD_SITE,
                    newPiece.QadPiece.LD_STATUS,
                    newPiece.QadPiece.LD_QTY_ALL.ToString(),
                    newPiece.Group,
                    newPiece.IsConsignmnet);
            }
            TaskCompleted();
        }
    }
}
