﻿using StockStatus.DataManage;
using StockStatus.MVC.Tasks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace StockStatus.MVC
{
    public class MainController: ControllerTasksCore
    {
        public StockStatusM StockData { get;  set; }
        private MainWindowViewMode view;
        public MainController()
        {
            StockData = new StockStatusM();
            StockData.Graph = new ObservableCollection<StockData>();
            StockData.Table = new ObservableCollection<StockDataTable>();
            BindingOperations.EnableCollectionSynchronization(StockData.Graph, StockData.GraphLock);
            BindingOperations.EnableCollectionSynchronization(StockData.Table, StockData.TableLock);
        }

        public void ViewLoaded(MainWindowViewMode view)
        {
            this.view = view;
        }

        public void UpdateStock()
        {
            AddTask(new StockDataLoader(this));
        }

        public void DataLoaded()
        {
            this.view.ViewData();
        }

        public void SaveReport(string selectedPath)
        {
            AddTask(new SaveStockStatusReport(this, selectedPath));
        }

        public void ReportExported()
        {
            this.view.ExportFinished();
        }
    }
}
