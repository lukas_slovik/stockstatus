﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.MVC
{
    public class LockedRunnableList
    {
        private List<Runnable> runnableList = new List<Runnable>();
        private object listLock = new object();
        public void Add(Runnable task)
        {
            lock (this.listLock)
            {
                this.runnableList.Add(task);
            }
        }
        public void Remove(Runnable task)
        {
            lock (this.listLock)
            {
                if (task != null)
                    this.runnableList.Remove(task);
            }

        }
        public void StopAllRunningTasks()
        {
            lock (this.listLock)
            {
                foreach (Runnable task in this.runnableList)
                {
                    task.StopTask();
                }
            }
        }
    }
}
