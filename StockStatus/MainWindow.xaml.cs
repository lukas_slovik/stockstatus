﻿using StockStatus.MVC;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace StockStatus
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MainWindowViewMode
    {
        private MainController controller;
        private object graphLock;
        public MainWindow()
        {
            this.graphLock = new object();
            this.controller = new MainController();
            InitializeComponent();
            this.DataContext = this.controller.StockData;
        }

        public void ViewData()
        {
            this.Dispatcher.Invoke(() =>
            {
                this.updateBtn.Content = "Update stock";
                this.updateBtn.IsEnabled = true;
                this.exportBtn.IsEnabled = true;
                this.stockStatusData.Visibility = Visibility.Visible;
                this.mainChart.Visibility = Visibility.Visible;
            });
        }

        public void ExportFinished()
        {
            this.Dispatcher.Invoke(() =>
            {
                this.exportBtn.Content = "Export";
                this.exportBtn.IsEnabled = true;
            });
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.controller != null)
            {
                this.updateBtn.Content = "Updating stock";
                this.updateBtn.IsEnabled = false;
                this.stockStatusData.Visibility = Visibility.Collapsed;
                this.mainChart.Visibility = Visibility.Collapsed;
                this.controller.UpdateStock();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.exportPathTb.Text = Properties.Settings.Default.DefaultExportPath;
            this.stockStatusData.Visibility = Visibility.Collapsed;
            this.mainChart.Visibility = Visibility.Collapsed;
            this.exportBtn.IsEnabled = false;
            if (this.controller != null)
                this.controller.ViewLoaded(this);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (this.controller != null)
                this.controller.StopController();
        }

        private void exportBtn_Click(object sender, RoutedEventArgs e)
        {
            string selectedPath = null;
            if (this.exportPathTb.Text != "")
            {
                Properties.Settings.Default.DefaultExportPath = this.exportPathTb.Text;
                Properties.Settings.Default.Save();
                this.controller.SaveReport(this.exportPathTb.Text);
            }
            else
            {
                using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
                {
                    System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        this.exportBtn.Content = "Exporting";
                        this.exportBtn.IsEnabled = false;
                        selectedPath = dialog.SelectedPath;
                        this.controller.SaveReport(selectedPath);
                    }
                }
            }
        }
    }
}
