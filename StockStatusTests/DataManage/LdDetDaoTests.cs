﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockStatus.DataManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage.Tests
{
    [TestClass()]
    public class LdDetDaoTests
    {
        [TestMethod()]
        public void StockDataTest()
        {
            Assert.IsNotNull(LdDetDao.Instance.StockData());
        }

        [TestMethod()]
        public void ClearedStockDataTest()
        {
            List<LD_DET> stock= LdDetDao.Instance.ClearedStockData();
            Assert.IsNotNull(stock);
        }
    }
}