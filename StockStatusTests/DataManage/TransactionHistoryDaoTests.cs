﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockStatus.DataManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage.Tests
{
    [TestClass()]
    public class TransactionHistoryDaoTests
    {
        [TestMethod()]
        public void IsConsignmentTestNull()
        {
            TransactionHistoryDao history = new TransactionHistoryDao(null);
            Assert.IsNull(history.IsConsignment());
        }

        [TestMethod()]
        public void IsConsignmentTestEmpty()
        {
            TransactionHistoryDao history = new TransactionHistoryDao(new LD_DET());
            Assert.IsNull(history.IsConsignment());
        }

        [TestMethod()]
        public void IsConsignmentTestExisting()
        {
            LD_DET piece = new LD_DET();
            piece.LD_PART = "2415330-W8D13";
            piece.LD_LOT = "801712";
            piece.LD_REF = "25079413";
            TransactionHistoryDao history = new TransactionHistoryDao(piece);
            Assert.IsTrue((bool)history.IsConsignment());
        }
    }
}