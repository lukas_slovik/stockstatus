﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockStatus.DataManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockStatus.DataManage.Tests
{
    [TestClass()]
    public class ItemNumberDaoTests
    {
        [TestMethod()]
        public void ItemsTest()
        {
            Assert.IsNotNull(ItemNumberDao.Instance.Items());
            Assert.IsTrue(ItemNumberDao.Instance.Items().Count > 0);
        }

        [TestMethod()]
        public void ItemGroupTestNull()
        {
            Assert.IsTrue(ItemNumberDao.Instance.ItemGroup(null) == "");
        }

        [TestMethod()]
        public void ItemGroupTestEmpty()
        {
            Assert.IsTrue(ItemNumberDao.Instance.ItemGroup("") == "");
        }

        [TestMethod()]
        public void ItemGroupTestExisting()
        {
            Assert.IsTrue(ItemNumberDao.Instance.ItemGroup("2329165-8A87") == "YARN");
        }

        [TestMethod()]
        public void GetByItemNumberTestNull()
        {
            Assert.IsNull(ItemNumberDao.Instance.GetByItemNumber(null));
        }

        [TestMethod()]
        public void GetByItemNumberTestEmpty()
        {
            Assert.IsNull(ItemNumberDao.Instance.GetByItemNumber(""));
        }

        [TestMethod()]
        public void GetByItemNumberTestExisting()
        {
            Assert.IsNotNull(ItemNumberDao.Instance.GetByItemNumber("2329165-8A87"));
        }
    }
}